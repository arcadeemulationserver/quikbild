# QuikBild

![](screenshot.png)

Contributors: MisterE, Zughy (hotbar background)

Code snippets from <https://gitlab.com/zughy-friends-minetest/arena_lib/-/blob/master/DOCS.md> and from Zughy's minigames.

License: GPL v3
Sounds: CC0

## Gameplay

Each player gets a turn to be the artist. In each round, the artist has a limited time to build a representation of a certain word using the tools they are given.

Other players will try to be the first to guess the word by sending it in chat (this does not spam global chat). Send chat messages with keywords. Use lowercase only, and spell them correctly!

The artist and the guesser get a point if the guesser guesses correctly. If no one guesses correctly, no one gets points that round. 

When all players had their turn as an artist, the game ends. The player (or players) with the most points win the game, provided they have at least 1 point.

## Basic setup

1) In chat, type `/arenas create quikbild <arena_name>`
2) Make your arena. There should be a central cage for the artist to build in, and a viewing area around it. The entire thing should prevent escape. Use glass or invisible barriers.
3) Type `/arenas edit quikbild <arena_name>`
4) Use the editor to place a minigame sign, assign it to your minigame.
5) While in the editor, move to where your arena will be.
6) Using the editor tools, mark player spawner locations. These should be placed in the viewing area. Protect the arena.
7) In 'QuikBild positions', set the build area. Use leftclick to set the first (pos1), rightclick for the 2nd corner (pos2). The build area will be cleared between rounds.
8) Set the artist spawn position. This should be a location inside the build area.
9) (Optional) Select a word list file in the arena properties. The default list is already OK but if you want, you can write your own.
10) In the arena properties, set the build time. This is the time allowed for the artist to build, and the time allowed for other players to guess. If this runs out, no one gets any points.
11) Exit the editor mode.
12) Type `/arenas settings quikbild`
13) Change the return spawn point to be next to the signs, and adjust the queuing time as desired.


# textdomain: quikbild
# author(s):
# reviewer(s):


##[ commands.lua ]##
language code=
Set Quickbild language=
Show a Quikbild language selection dialog to a player=
Chat is disabled while being the artist in quikbild. Don't cheat!=
player=

##[ hud.lua ]##
Your Score: @1=

##[ init.lua ]##
QuikBild=

##[ items.lua ]##
Choose Language=
Help=
QuikBild Help=
Each player gets a turn to be Builder. Everyone else guesses what the Builder is building. To guess type your guess in chat. Use lowercase letters only. Answers can be 1 or 2 words. When it is your turn to be Builder you will be shown a word - build it. DO NOT build letters and do not try to tell other players what the word is. That ruins the game for everyone. No one will want to play this with you anymore. If you do not know the word use a search engine to look it up. You have time to do that!=

##[ minigame_manager.lua ]##
No one got any points=
Try again!=
You are the Artist. Build the word you see=
@1 is the artist.=
Guess what they are building.=
Oops! The artist left the game.=
Round begins in @1=
BUILD!=
BEGIN GUESSING!=
Oops! The artist left the game. The word was @1=
TIME's UP! The word was: @1=
WORD: @1=
TIME: @1=
TIME LEFT IN ROUND: @1=
Correct!=
You got it!=
Way to go!=
Outstanding!=
Yay!=
+1 pt=
The word was: @1=
@1 guessed your word.=
@1 guessed the word. Round over!=
Language setting confirmed!=

##[ nodes.lua ]##
Quikbild Climb-able Node=
Black Block=
Blue Block=
Brown Block=
Cyan Block=
Dark Green Block=
Dark Grey Block=
Green Block=
Grey Block=
Magenta Block=
Orange Block=
Pink Block=
Red Block=
Violet Block=
White Block=
Yellow Block=

##[ privs.lua ]##
With this you can use /quikbild create <arena_name>,/quikbild edit <arena_name>=

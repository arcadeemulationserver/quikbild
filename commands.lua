local S = core.get_translator("quikbild")


core.register_chatcommand("qblang", {
  params = "[<"..S("language code")..">]",
  description = S("Set Quickbild language"),
  func = function(name, param)
      local player = core.get_player_by_name(name)
      if not player or not player:is_player() then
          return false, S("No player.")
      end
      if param == "" then
         quikbild.send_lang_fs(name)
      elseif quikbild.languages[param] then
          quikbild.set_language(player, quikbild.languages[param].number, true)
      else
          local codes = {}
          for i=1, #quikbild.languages_by_number do
              table.insert(codes, quikbild.languages_by_number[i].code)
          end
          local codestr = table.concat(codes, ", ")
          return false, S("Incorrect language code. Available codes: @1", codestr)
      end

      return true
  end,
})



core.register_chatcommand("qblang_send", {
  params = "<"..S("player")..">",
  privs = { quikbild_admin = true },
  description = S("Show a Quikbild language selection dialog to a player"),
  func = function(name, param)
      if param == "" then
        return false
      end
      if core.get_player_by_name(param) then
        quikbild.send_lang_fs(param)
        return true
      else
        return false, S("Player is not online.")
      end
  end,
})

core.register_on_mods_loaded( function()
  table.insert(core.registered_on_chat_messages,1,function(name, message)
    local mod = arena_lib.get_mod_by_player(name)
    if mod == "quikbild" then
      local arena = arena_lib.get_arena_by_player(name)
      if arena then
        if arena.artist and arena.artist == name and arena.in_game and arena.in_celebration == false then
          if message ~= "/quit" then
            core.chat_send_player(name, core.colorize("#eea160","[!]" .. S("Chat is disabled while being the artist in quikbild. Don't cheat!")))
            return true
          end
        end
      end
    end
  end)
end)

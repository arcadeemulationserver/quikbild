local S = core.get_translator("quikbild")

-- Waypoints to show the positions

local waypoints = {} -- Key: player name; value: list of waypoint IDs

-- Waypoint icons
local wp_icons = {
	["artist_spawn_pos"] = "quikbild_editor_artist_spawn.png",
	["build_area_pos_1"] = "quikbild_editor_wp_build_pos_1.png",
	["build_area_pos_2"] = "quikbild_editor_wp_build_pos_2.png",
}

local function remove_waypoints(p_name, arena)
	local player = core.get_player_by_name(p_name)
	if player and waypoints[p_name] then
		for _, waypoint_ID in pairs(waypoints[p_name]) do
			player:hud_remove(waypoint_ID)
		end
	end
	waypoints[p_name] = nil
end

local function show_waypoints(p_name, arena)
	if waypoints[p_name] then
		remove_waypoints(p_name)
	end
	if not quikbild.check_positions(arena) then
		return
	end
	local player = core.get_player_by_name(p_name)
	if not player or not player:is_player() then
		return
	end
	waypoints[p_name] = {}

	local function is_valid(propname)
		local prop = arena[propname]
		if type(prop) ~= "table" or type(prop.x) ~= "number" or type(prop.y) ~= "number" or type(prop.z) ~= "number" then
			return false
		else
			return true
		end
	end
	local function show_waypoint(player, propname)
		local prop = arena[propname]
		local hid = player:hud_add({
			hud_elem_type = "image_waypoint",
			text = wp_icons[propname],
			world_pos = prop,
			scale = {x = 5, y = 5}
		})
		table.insert(waypoints[p_name], hid)
	end

	do
		if is_valid("artist_spawn_pos") then
			show_waypoint(player, "artist_spawn_pos")
		end

		if is_valid("build_area_pos_1") and is_valid("build_area_pos_2") then
			if vector.equals(arena.build_area_pos_1, arena.build_area_pos_2) then
				local hid = player:hud_add({
					hud_elem_type = "image_waypoint",
					text = "quikbild_editor_wp_build_pos_both.png",
					world_pos = arena.build_area_pos_1,
					scale = {x = 5, y = 5},
				})
				table.insert(waypoints[p_name], hid)
			else
				show_waypoint(player, "build_area_pos_1")
				show_waypoint(player, "build_area_pos_2")
			end
		end
	end
end

arena_lib.on_join_editor("quikbild", function(arena, p_name)
	show_waypoints(p_name, arena)
end)
arena_lib.on_leave_editor("quikbild", function(arena, p_name)
	remove_waypoints(p_name, arena)
end)

-- Add editor items

local get_set_pos_function = function(property_name, round)
	return function(itemstack, user, pointed_thing)
		local umeta = user:get_meta()
		local uname = user:get_player_name()
		local mod = umeta:get_string("arena_lib_editor.mod")
		local arena_name = umeta:get_string("arena_lib_editor.arena")
		local _, arena = arena_lib.get_arena_by_name(mod, arena_name)

		local newpos = user:get_pos()
		if round then
			newpos = vector.round(newpos)
		end
		arena_lib.change_arena_property(uname, mod, arena_name, property_name, dump(newpos), true)
		show_waypoints(uname, arena)
	end
end

core.register_craftitem("quikbild:editor_build_area_set", {
	description = S("Build area (LMB: pos1, RMB: pos2)"),
	inventory_image = "quikbild_editor_build_area_set.png",
	wield_image = "quikbild_editor_build_area_set.png",
	groups = { not_in_creative_inventory = 1 },
	stack_max = 1,
	on_drop = function() end,

	on_use = get_set_pos_function("build_area_pos_1", true),
	on_secondary_use = get_set_pos_function("build_area_pos_2", true),
	on_place = get_set_pos_function("build_area_pos_2", true),
})

core.register_craftitem("quikbild:editor_artist_spawn_pos_set", {
	description = S("Artist spawn position"),
	inventory_image = "quikbild_editor_artist_spawn.png",
	wield_image = "quikbild_editor_artist_spawn.png",
	groups = { not_in_creative_inventory = 1 },
	stack_max = 1,
	on_drop = function() end,
	on_place = function() end,

	on_use = get_set_pos_function("artist_spawn_pos", false),
})


-- Add editor section

arena_lib.register_editor_section("quikbild", {
	name = S("QuikBild positions"),
	icon = "magiccompass_quikbild.png",
	give_items = function(itemstack, user, arena)
		return {
			"quikbild:editor_build_area_set",
			"quikbild:editor_artist_spawn_pos_set",
		}
	end,
})

